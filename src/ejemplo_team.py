import requests
import json
import os

rapid_key = os.environ["x_rapidapi_key"]
headers = {
    "x-rapidapi-host": "v3.football.api-sports.io",
    "x-rapidapi-key": rapid_key,
}
team_id = "2295"
league_id = "262"
conn = requests.get(
    "http://v3.football.api-sports.io/teams/statistics?",
    params={"season": "2019", "team": team_id, "league": league_id},
    headers=headers,
)

res = conn.json()
with open(f"results/data_file_{league_id}_{team_id}.json", "w") as write_file:
    json.dump(res, write_file)
