from os import listdir
from os.path import isfile, join
import json
import requests
from futbol import get_rapidkey, get_headers
import time
import typer

giffm = typer.Typer(help="Awesome CLI user manager.")


@giffm.command()
def get_statistics_players_match(number_round: int = 1):
    league_raw = "results/data_file_140_2020.json"
    league = read_json(league_raw)

    match_ids = get_id_played_match(league)
    downladed_match = downladed_file()
    not_downloaded_yet = [id for id in match_ids if id not in downladed_match]

    rapidapi_key = get_rapidkey()
    headers = get_headers()
    for match in not_downloaded_yet:
        print(match)
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/statistics?fixture={match}",
            headers=headers,
        )
        res = conn.json()
        with open(f"results/La_Liga_2020/data_statisitcs_{match}.json", "w") as write_file:
            json.dump(res, write_file)


def downladed_file():
    my_path = "results/La_Liga_2020"
    onlyfiles = [f for f in listdir(my_path) if isfile(join(my_path, f))]
    fixture = [int(f.split(".")[0].split("_")[-1]) for f in onlyfiles]
    return fixture


def read_json(file_name):
    f = open(
        file_name,
    )
    league = json.load(f)
    f.close()
    return league


def get_id_played_match(league):
    ids = [
        match["fixture"]["id"]
        for match in league["response"]
        if match["fixture"]["status"]["long"] == "Match Finished"
    ]
    return ids


if __name__ == "__main__":
    giffm()
