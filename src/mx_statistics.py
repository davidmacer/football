from futbol import League
import json

league_season = "262_2021"
league_path = f"results/data_file_{league_season}.json"
league_file = open(league_path)
statistic_of_league = json.load(league_file)
league_file.close()


def read_statistic_match(match_path):
    match_file = open(match_path)
    statistic_of_match = json.load(match_file)
    match_file.close()
    return statistic_of_match


match_ids = [
    match["fixture"]["id"]
    for match in statistic_of_league["response"]
    if match["fixture"]["status"]["long"] == "Match Finished"
]

print(match_ids)
league = League(statistic_of_league)
for match_id in match_ids:
    print(match_id)
    path_match = f"results/MX_2021/data_statisitcs_{match_id}.json"
    statistic_of_match = read_statistic_match(path_match)
    league.append_match_statistic(statistic_of_match)
league.write_statistics(f"results/statistics_{league_season}.csv")
