import requests
import json
from futbol import get_rapidkey, get_headers
import typer

rapidapi_key = get_rapidkey()
headers = get_headers()

season = "2020"
league = "262"

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def get_topscorers(season: int = 2020, league: int = 262):
    conn = requests.get(
        "https://v3.football.api-sports.io/players/topscorers",
        params={"season": season, "league": league},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/top_scorers_{league}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


if __name__ == "__main__":
    app()
