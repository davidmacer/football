import requests
import json
from futbol import get_rapidkey, get_headers

rapidapi_key = get_rapidkey()

headers = get_headers()

conn = requests.get(
    "https://v3.football.api-sports.io/leagues",
    headers=headers,
)
res = conn.json()
with open("results/data_file_league_season.json", "w") as write_file:
    json.dump(res, write_file)
