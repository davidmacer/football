from futbol import League, downladed_file, read_statistic_match, read_json

league_season = "78_2020"
league_path = f"results/data_file_{league_season}.json"
statistic_of_league = read_json(league_path)


path_results = "results/Bundesliga_2020"
match_ids = downladed_file(path_results)

print(match_ids)
league = League(statistic_of_league)
for match_id in match_ids:
    print(match_id)
    path_match = f"{path_results}/data_statisitcs_{match_id}.json"
    statistic_of_match = read_statistic_match(path_match)
    league.append_match_statistic(statistic_of_match)
league.write_statistics(f"results/statistics_{league_season}.csv")
