from futbol import (
    get_rapidkey,
    get_headers,
    Downloader_German_Matches,
    Downloader_German_Players,
    read_json,
    get_id_played_match,
    downladed_file,
)
import time


def get_statistics_players_match(for_download):
    headers = get_headers()
    downloader_matches = Downloader_German_Matches()
    downloader_players = Downloader_German_Players()
    for match in for_download:
        print(match)
        downloader_matches.get_request(match)
        downloader_matches.write_match()
        downloader_players.get_request(match)
        downloader_players.write_players()


if __name__ == "__main__":
    league_raw = "results/data_file_78_2020.json"
    league = read_json(league_raw)

    match_ids = get_id_played_match(league)
    downladed_match = downladed_file("results/Bundesliga_2020")
    not_downloaded_yet = [id for id in match_ids if id not in downladed_match]

    get_statistics_players_match(not_downloaded_yet)
