import json
import requests
from futbol import get_rapidkey, get_headers
import time
import typer

giffm = typer.Typer(help="Awesome CLI user manager.")


@giffm.command()
def get_statistics_players_match(number_round: int = 1):
    league_raw = "results/data_file_262_2021.json"
    f = open(
        league_raw,
    )
    league = json.load(f)
    f.close()

    match_ids = []
    round_fixture = f"Apertura - {number_round}"
    print(round_fixture)
    for match in league["response"]:
        was_played = match["fixture"]["status"]["long"] == "Match Finished"
        was_round = match["league"]["round"] == round_fixture
        interesed_match = was_played and was_round
        if interesed_match:
            match_ids.append(match["fixture"]["id"])

    rapidapi_key = get_rapidkey()

    headers = get_headers()

    for match in match_ids:
        print(match)
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/statistics?fixture={match}",
            headers=headers,
        )
        res = conn.json()
        with open(f"results/MX_2021/data_statisitcs_{match}.json", "w") as write_file:
            json.dump(res, write_file)

        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/players?fixture={match}",
            headers=headers,
        )

        res = conn.json()
        with open(f"results/MX_2021/data_statisitcs_players_{match}.json", "w") as write_file:
            json.dump(res, write_file)


if __name__ == "__main__":
    giffm()
