import os
from os import listdir
from os.path import isfile, join
import json
import requests


def get_rapidkey():
    rapidapi_key = os.environ["x_rapidapi_key"]
    return rapidapi_key


def get_headers():
    rapidapi_key = get_rapidkey()
    headers = {"x-rapidapi-host": "v3.football.api-sports.io", "x-rapidapi-key": rapidapi_key}
    return headers


class Downloader_German_Matches:
    def __init__(self):
        self.headers = get_headers()
        self.answer = None
        self.match = None

    def get_request(self, match):
        self.match = match
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/statistics?fixture={self.match}",
            headers=self.headers,
        )
        self.answer = conn.json()

    def write_match(self):
        with open(f"results/Bundesliga_2020/data_statisitcs_{self.match}.json", "w") as write_file:
            json.dump(self.answer, write_file)


class Downloader_German_Players:
    def __init__(self):
        self.headers = get_headers()
        self.answer = None
        self.match = None

    def get_request(self, match):
        self.match = match
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/players?fixture={self.match}",
            headers=self.headers,
        )
        self.answer = conn.json()

    def write_players(self):
        with open(
            f"results/Bundesliga_2020/data_statisitcs_players_{self.match}.json", "w"
        ) as write_file:
            json.dump(self.answer, write_file)


def read_json(file_name):
    f = open(
        file_name,
    )
    league = json.load(f)
    f.close()
    return league


def get_id_played_match(league):
    ids = [
        match["fixture"]["id"]
        for match in league["response"]
        if match["fixture"]["status"]["long"] == "Match Finished"
    ]
    return ids


def downladed_file(my_path):
    onlyfiles = [f for f in listdir(my_path) if isfile(join(my_path, f))]
    fixture = [int(f.split(".")[0].split("_")[-1]) for f in onlyfiles]
    return fixture


def read_statistic_match(match_path):
    match_file = open(match_path)
    statistic_of_match = json.load(match_file)
    match_file.close()
    return statistic_of_match
