"""Package to download data football"""

__version__ = "0.2.0"
from .ejemplo_liga import *  # noqa
from .calculate_tsr_from_play import Match  # noqa
from .calculate_tsr_from_play import League  # noqa
from .calculate_tsr_from_play import Team  # noqa
