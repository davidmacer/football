import pandas as pd


class Match:
    def __init__(self, statistic_of_match):
        self.id = int(statistic_of_match["parameters"]["fixture"])
        self.home_team = Team(statistic_of_match["response"][0])
        self.away_team = Team(statistic_of_match["response"][1])

    def summary(self):
        summary_match = {"match_id": self.id}
        summary_match.update(self.__home_summary())
        summary_match.update(self.__away_summary())
        return summary_match

    def __home_summary(self):
        home_summary = summary_to_home_summary(**self.home_team.summary())
        return home_summary

    def __away_summary(self):
        away_summary = summary_to_away_summary(**self.away_team.summary())
        return away_summary


def summary_to_home_summary(id, total_shots, shots_on_goal, shots_insidebox):
    home_summary = {
        "home_id": id,
        "home_total_shots": total_shots,
        "home_shots_on_goal": shots_on_goal,
        "home_shots_insidebox": shots_insidebox,
    }
    return home_summary


def summary_to_away_summary(id, total_shots, shots_on_goal, shots_insidebox):
    away_summary = {
        "away_id": id,
        "away_total_shots": total_shots,
        "away_shots_on_goal": shots_on_goal,
        "away_shots_insidebox": shots_insidebox,
    }
    return away_summary


class League:
    def __init__(self, statistic_of_league):
        self.id = self.__get_parameter(statistic_of_league, parameter="league")
        self.season = self.__get_parameter(statistic_of_league, parameter="season")
        self.id_matches = self.__get_id_matches(statistic_of_league)
        self.goals = self.__get_goals_for_matches(statistic_of_league)
        self.statistics = self.__empty_statistics()

    def fill_match_statistic(self, statistic_of_match):
        match = Match(statistic_of_match)
        summary_match = self.goals[match.id]
        summary_match.update(match.summary())
        return summary_match

    def append_match_statistic(self, match):
        summary_match = self.fill_match_statistic(match)
        self.statistics = self.statistics.append(summary_match, ignore_index=True)
        order_columns = [
            "home",
            "away",
            "match_id",
            "home_id",
            "home_total_shots",
            "home_shots_on_goal",
            "home_shots_insidebox",
            "away_id",
            "away_total_shots",
            "away_shots_on_goal",
            "away_shots_insidebox",
        ]
        self.statistics = self.statistics[order_columns]

    def write_statistics(self, output_name):
        self.statistics.to_csv(output_name, index=False)

    def __get_parameter(self, statistic_of_league, parameter):
        parameter = int(statistic_of_league["parameters"][parameter])
        return parameter

    def __get_id_matches(self, statistic_of_league):
        matches = statistic_of_league["response"]
        id_matches = []
        for match in matches:
            id_matches.append(match["fixture"]["id"])
        return id_matches

    def __get_goals_for_matches(self, statistic_of_league):
        matches = statistic_of_league["response"]
        goals_for_matches = {}
        for match in matches:
            goals_for_matches.update({match["fixture"]["id"]: match["goals"]})
        return goals_for_matches

    def __empty_statistics(self):
        empty = {
            "match_id": [],
            "home_id": [],
            "home_total_shots": [],
            "home_shots_on_goal": [],
            "away_id": [],
            "away_total_shots": [],
            "away_shots_on_goal": [],
            "home": [],
            "away": [],
        }
        statistics = pd.DataFrame(empty)
        return statistics


class Team:
    def __init__(self, statistic_team):
        self.id = statistic_team["team"]["id"]
        self.total_shots = self.get_total_shots(statistic_team)
        self.shots_on_goal = self.get_shots_on_goal(statistic_team)
        self.shots_insidebox = self.get_shots_insidebox(statistic_team)

    def get_total_shots(self, statistic_team):
        total_shots = self.__get_shots(statistic_team, index=2)
        return total_shots

    def get_shots_on_goal(self, statistic_team):
        shots_on_goal = self.__get_shots(statistic_team, index=0)
        return shots_on_goal

    def get_shots_insidebox(self, statistic_team):
        shots_insidebox = self.__get_shots(statistic_team, index=4)
        return shots_insidebox

    def summary(self):
        return {
            "id": self.id,
            "total_shots": self.total_shots,
            "shots_on_goal": self.shots_on_goal,
            "shots_insidebox": self.shots_insidebox,
        }

    def __get_shots(self, statistic_team, index):
        statistic = statistic_team["statistics"][index]
        shots = statistic["value"]
        return shots
