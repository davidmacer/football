from typer.testing import CliRunner

from ..src.get_top_score_liga import app

runner = CliRunner()


def test_app():
    result = runner.invoke(app, ["--help"])
    assert result.exit_code == 0
    assert "[default: 2020]" in result.stdout
    assert "--league INTEGER" in result.stdout


from ..src.get_id_fixture_from_mx import giffm


def test_giffm():
    result = runner.invoke(giffm, ["--help"])
    assert result.exit_code == 0
    assert "[default: 1]" in result.stdout
    assert "--number-round INTEGER" in result.stdout
