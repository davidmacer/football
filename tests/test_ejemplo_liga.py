from futbol import get_rapidkey, get_headers
import os


def test_get_rapidkey():
    obtained_key = get_rapidkey()
    expected_key = os.environ["x_rapidapi_key"]
    assert expected_key == obtained_key


def test_get_headers(mocker):
    def get_rapidkey():
        return "key"

    mocker.patch("futbol.ejemplo_liga.get_rapidkey", get_rapidkey)
    obtained_headers = get_headers()
    expected_headers = {"x-rapidapi-host": "v3.football.api-sports.io", "x-rapidapi-key": "key"}
    assert expected_headers == obtained_headers
