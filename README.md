# football

Este paquete sirve para hacer peticiones [api sport](https://api-sports.io/documentation/football/v3).

Para obtener la información de las ligas que hay en un país podemos utilizar
`src/info_from_code.py`. A partir de código del país ("MX" para México) obtenemos los ID de las
tres ligas.

Con ese ID podemos obtener la información del torneo a partir de `src/ejemplo_liga.py`.

Con el ID de la liga y del equipo podemos obtener la estadística del equipo con
`src/ejemplo_team.py`.

- El ID de la Euro es 4
- LaLiga tiene ID 140
- La Serie A ID 135
- UEFA Champions League es 2
- Copa América 9
- Bundesliga 78
