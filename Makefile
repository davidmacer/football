repo = futbol
codecov_token = ffcb865e-9822-446b-9387-ebf3c6ad30ed

results/data_file_262_2019.json: src/ejemplo_liga.py
	mkdir --parents results
	python src/ejemplo_liga.py

results/data_statisitcs_Monarcas_Atlas.json: src/ejemplo_stats_play.py
	mkdir --parents results
	python src/ejemplo_stats_play.py

results/statistics_262_2019.csv: src/league_statistics.py
	mkdir --parents results
	python src/league_statistics.py

.PHONY: check coverage format tests

check:
	black --check --line-length 100 ${repo}
	black --check --line-length 100 tests
	black --check --line-length 100 src
	flake8 --max-line-length 100 ${repo}
	flake8 --max-line-length 100 tests
	flake8 --max-line-length 100 src

clean:
	rm --force --recursive **/__pycache__
	rm --force --recursive __pycache__
	rm --force --recursive .pytest_cache
	rm --force --recursive results
	rm --force .mutmut-cache

coverage:
	mkdir --parents results
	pytest --cov=${repo} --cov-report=xml --verbose && \
	codecov --token=${codecov_token}

format:
	black --line-length=100 ${repo}
	black --line-length=100 src
	black --line-length=100 tests

install:
	pip install .

mutants: install
	mkdir --parents results
	mutmut run --paths-to-mutate ${repo}

test_cli:
	shellspec

tests: install
	mkdir --parents results
	pytest --verbose