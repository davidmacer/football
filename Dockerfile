FROM python:3.8
WORKDIR /workdir
COPY . .
RUN pip install \
    . \
    black \
    codecov \
    flake8 \
    mutmut \
    pylint \
    pytest \
    pytest-cov \
    pytest-mock \
    rope \
    typer
RUN curl -fsSL https://git.io/shellspec | sh -s -- --yes
ENV PATH="/root/.local/lib/shellspec:$PATH"
CMD make
